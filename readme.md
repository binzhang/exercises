## StackOverflow exercise ##

**Please find the slides in docs dir.**

There are two implementations executable by :

1. _stackoverflow.StackOverflow.scala_

    This implementation follows the instruction with minor changes.

2. _stackoverflow.Application.scala_

    The second implementation attempts to adapt Strategy Pattern. Preprocessing of the datasource to a vector can be extended by overriding the _com.company.analytics.preprocessors.PreprocessingExecutor_ trait, while the k-means can be revised by extending _com.company.analytics.algorithm.clustering.ClusteringStrategy_.

    The current implementation has a CSV preprocessor and a k-means clustering. They can be used by the client with:

```
    def loadData():RDD[String]   = sc.textFile("src/main/resources/stackoverflow/stackoverflow.csv")
    val preprocessor = new Preprocessor() with CSVPreprocessingExecutor
    val vectors = preprocessor.getVectors(loadData,langs=langs, langSpread=langSpread)
    val kmeans = new KMeans() with KMeansStrategy
    val means =kmeans.train(sampleVectors(vectors),vectors,kmeansEta,kmeansMaxIterations,kmeansKernels,false)
```
   The preprocessor's getVectors function expects a function to load data from sources as an input argument, it invokes the function to get the RDD[String], then performs preprocessing of the RDD.
   The postprocessing stage, converting the result and printing the result are still handled by the client.

   The snapshot shows the clustering result when K=45 and langspread=5000

```
Resulting clusters:
  Score  Dominant language (%percent)  Questions
================================================
      0  MATLAB            (100.0%)         3725
      1  CSS               (100.0%)       113598
      1  C#                (100.0%)       361835
      1  PHP               (100.0%)       315734
      1  Groovy            (100.0%)         2729
      1  Java              (100.0%)       383473
      1  JavaScript        (100.0%)       365649
      1  Objective-C       (100.0%)        94745
      1  Ruby              (100.0%)        54727
      2  Clojure           (100.0%)         3324
      2  Python            (100.0%)       174586
      2  C++               (100.0%)       181258
      2  Scala             (100.0%)        12423
      2  MATLAB            (100.0%)         7989
      2  Perl              (100.0%)        19229
      4  Haskell           (100.0%)        10362
      5  MATLAB            (100.0%)         2774
      9  Perl              (100.0%)         4716
     10  Groovy            (100.0%)          310
     12  Clojure           (100.0%)          712
     25  Scala             (100.0%)          728
     53  Haskell           (100.0%)          202
     61  Groovy            (100.0%)           14
     66  Clojure           (100.0%)           57
     78  Perl              (100.0%)           56
     79  C#                (100.0%)         2585
     85  Ruby              (100.0%)          648
     97  Objective-C       (100.0%)          784
    130  Scala             (100.0%)           47
    135  PHP               (100.0%)          512
    172  CSS               (100.0%)          358
    215  C++               (100.0%)          261
    227  Python            (100.0%)          400
    249  Java              (100.0%)          483
    377  JavaScript        (100.0%)          431
    443  C#                (100.0%)          147
    503  Objective-C       (100.0%)           73
    546  Ruby              (100.0%)           34
    766  CSS               (100.0%)           26
    887  PHP               (100.0%)           13
   1130  Haskell           (100.0%)            2
   1269  Python            (100.0%)           19
   1290  C++               (100.0%)            9
   1895  JavaScript        (100.0%)           33
  10271  Java              (100.0%)            2
Processing took 82932 ms.
```

Note that the implementation 1 and implementation 2 get slightly different results due to the random selection of the sampling vector, and the order of the input vector RDD to the getSampleVectors function.
