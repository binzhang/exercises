package com.company.analytics.preprocessors

/**
  * Created by zhang on 6/18/17.
  */
object PreprocessorHelper {

  def firstLangInTag(tag: Option[String], ls: List[String]): Option[Int] = {
    if (tag.isEmpty) None
    else if (ls.isEmpty) None
    else if (tag.get == ls.head) Some(0) // index: 0
    else {
      val tmp = firstLangInTag(tag, ls.tail)
      tmp match {
        case None => None
        case Some(i) => Some(i + 1) // index i in ls.tail => index i+1
      }
    }
  }
}
