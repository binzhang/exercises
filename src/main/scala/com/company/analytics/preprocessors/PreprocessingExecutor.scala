package com.company.analytics.proprocessors

import com.company.analytics.models.Posting
import com.company.analytics.preprocessors.PreprocessorHelper
import org.apache.spark.{RangePartitioner, HashPartitioner}
import org.apache.spark.rdd.RDD

/**
  * Created by zhang on 6/17/17.
  */
trait PreprocessingExecutor {

  protected def rawPostings(lines: RDD[String]): RDD[Posting]
  def getVectors(loadData: ()=>RDD[String],langs:List[String],langSpread:Int):RDD[(Int, Int)]
  /** Compute the vectors for the kmeans */
  def vectorPostings(scored: RDD[(Posting, Int)], langs:List[String],langSpread:Int): RDD[(Int, Int)] = {
    /** Return optional index of first language that occurs in `tags`. */


    scored.flatMap((questionAndScore) => {
      val question = questionAndScore._1
      val tag = question.tags
      val index = PreprocessorHelper.firstLangInTag(tag,langs)
      index match {
        case None  => None
        case Some(v) =>  Some(index.get*langSpread,questionAndScore._2)
      }
    })
      .persist()
  }



  protected def getScoredPostings(postings: RDD[Posting]): RDD[(Posting, Int)]={
    postings.persist()
    val questionPostings = postings.filter(_.postingType==1).map(posting=> (posting.id,posting))
    val answerPostings = postings.filter((post)=>(post.postingType==2) && (post.parentId!=None))
      .map(posting=>(posting.parentId.get, posting))

    .partitionBy(new HashPartitioner(8)).persist // repartition so that reduce and join are less expensive
    postings.unpersist()
    val answerPostingsWithMaxSocre = answerPostings
      .reduceByKey((posting1,posting2)=>{
        if(posting1.score> posting2.score){
          posting1
        }else{
          posting2
        }
      }) // get question ID and answer with max score

    val scoredPostings = questionPostings.join(answerPostingsWithMaxSocre).
      map(questionAndAnswerWithMaxScore => {
        val question = questionAndAnswerWithMaxScore._2._1
        val answer = questionAndAnswerWithMaxScore._2._2
        (question,answer.score)
      })
    answerPostings.unpersist()
    scoredPostings
  }
}

trait CSVPreprocessingExecutor extends PreprocessingExecutor{

  protected def rawPostings(lines: RDD[String]): RDD[Posting] =
    lines.map(line => {
      val arr = line.split(",")
      Posting(postingType =    arr(0).toInt,
        id =             arr(1).toInt,
        acceptedAnswer = if (arr(2) == "") None else Some(arr(2).toInt),
        parentId =       if (arr(3) == "") None else Some(arr(3).toInt),
        score =          arr(4).toInt,
        tags =           if (arr.length >= 6) Some(arr(5).intern()) else None)
    })
  override def getVectors(loadData: ()=>RDD[String],langs:List[String],langSpread:Int):RDD[(Int, Int)]={
    val lines = loadData()
    val raw     = rawPostings(lines)
    val scored = getScoredPostings(raw)
    vectorPostings(scored,langs,langSpread)
  }
}
