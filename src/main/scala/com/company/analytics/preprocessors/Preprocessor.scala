package com.company.analytics.proprocessors

import org.apache.spark.rdd.RDD

/**
  * Created by zhang on 6/17/17.
  */
class Preprocessor extends Serializable{

  self: PreprocessingExecutor=>

  def getVectors (loadData: ()=>RDD[String],langs:List[String],langSpread:Int): RDD[(Int, Int)]={

    getVectors(loadData,langs,langSpread)
  }
}
