package com.company.analytics.algorithm.clustering

import org.apache.spark.RangePartitioner
import org.apache.spark.rdd.RDD

import scala.annotation.tailrec


/**
  * Created by zhang on 6/18/17.
  */
trait ClusteringStrategy {

  protected def kmeans(means: Array[(Int, Int)], vectors: RDD[(Int, Int)], convergenceCriteria: Double, numberOfClusters:Int,maxIteration:Int, debug: Boolean = false): Array[(Int, Int)]

}

trait KMeansStrategy extends ClusteringStrategy{

  var kmeansEta :Double = 0.0

  var kmeansKernels:Int = 0

  var kmeansMaxIterations:Int = 0

  override protected def kmeans(means: Array[(Int, Int)], vectors: RDD[(Int, Int)], convergenceCriteria: Double, numberOfClusters:Int,maxIteration:Int, debug: Boolean = false):Array[(Int,Int)] = {

    kmeansEta=convergenceCriteria

    kmeansKernels = numberOfClusters

    kmeansMaxIterations = maxIteration

    kmeansAlgo(means,vectors,debug = debug)
  }
  @tailrec protected final def kmeansAlgo(means: Array[(Int, Int)], vectors: RDD[(Int, Int)], iter: Int = 1, debug: Boolean = false): Array[(Int, Int)] = {
    // Fill in the newMeans array
    val newMeansPoints = vectors.map(point =>{
      (KMeansHelper.findClosest(point,centers = means),(point._1.toLong,point._2.toLong,1))
    })

    val newMeansWithAvg = newMeansPoints
        .reduceByKey((pointAndCount1,pointAndCount2)=>{

          val sumX = pointAndCount1._1+pointAndCount2._1
          val sumY = pointAndCount1._2+pointAndCount2._2
          val count = pointAndCount1._3+pointAndCount2._3
          (sumX,sumY,count)
        })
      .mapValues(pointSumAndCount=>((pointSumAndCount._1/pointSumAndCount._3).toInt,(pointSumAndCount._2/pointSumAndCount._3).toInt))
      //.groupByKey()
      //.mapValues(points => KMeansHelper.averageVectors(points))
      .collect()
    val indexHelper = newMeansWithAvg.map(_._1)
    //
    val newMeans = means.zipWithIndex.map(valueWithIndex=>{
      val indexOfNewMeans = indexHelper.indexOf(valueWithIndex._2)
      if(indexOfNewMeans == -1){
        (valueWithIndex._2,valueWithIndex._1) // assign the old centroid if the new centroid does not have the index
      }else{
        newMeansWithAvg(indexOfNewMeans) // otherwise, the new centroid becomes the centroid in that index
      }
    }).map(_._2) // means and newMeans must have the same size

    val distance = KMeansHelper.euclideanDistance(means,newMeans)
    //.euclideanDistance(means, newMeans)

    if (debug) {
      println(
        s"""Iteration: $iter

            |  * current distance: $distance

            |  * desired distance: $kmeansEta
            |  * means:""".stripMargin)
      for (idx <- 0 until kmeansKernels)
        println(f"   ${means(idx).toString}%20s ==> ${newMeans(idx).toString}%20s  " +
          f"  distance: ${KMeansHelper.euclideanDistance(means(idx), newMeans(idx))}%8.0f")
    }

    if (KMeansHelper.converged(distance,kmeansEta))
      newMeans
    else if (iter < kmeansMaxIterations)
      kmeansAlgo(newMeans, vectors, iter + 1, debug)
    else {
      println(
        "Reached max iterations!")
      newMeans
    }
  }




}


