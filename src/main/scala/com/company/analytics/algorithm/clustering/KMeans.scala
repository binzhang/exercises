package com.company.analytics.algorithm.clustering

import org.apache.spark.rdd.RDD

/**
  * Created by zhang on 6/18/17.
  */
class KMeans {
  self:ClusteringStrategy=>

  def train(means: Array[(Int, Int)], vectors: RDD[(Int, Int)], convergenceCriteria:Double,maxIteration:Int, numberOfClusters:Int, debug: Boolean = false): Array[(Int, Int)]={

    kmeans(means, vectors, convergenceCriteria,numberOfClusters,maxIteration, debug)

  }

}
