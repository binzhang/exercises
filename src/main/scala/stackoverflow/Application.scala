package stackoverflow


import com.company.analytics.algorithm.clustering.{KMeans, KMeansHelper, KMeansStrategy}
import com.company.analytics.models.Posting
import com.company.analytics.proprocessors.{CSVPreprocessingExecutor, Preprocessor}
import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

/**
  * Created by zhang on 6/17/17.
  */
object Application extends Application{
  @transient lazy val conf: SparkConf = new SparkConf()
    .setMaster("local[2]")
    .setAppName("StackOverflow")
    .set("spark.serializer","org.apache.spark.serializer.KryoSerializer")
  conf.registerKryoClasses(Array(classOf[Posting]))
  @transient lazy val sc: SparkContext = new SparkContext(conf)

  /** Main function */
  def main(args: Array[String]): Unit = {

    val start = System.currentTimeMillis()

    def loadData():RDD[String]   = sc.textFile("src/main/resources/stackoverflow/stackoverflow.csv")
    val preprocessor = new Preprocessor() with CSVPreprocessingExecutor

    val vectors = preprocessor.getVectors(loadData,langs=langs, langSpread=langSpread)

    val kmeans = new KMeans() with KMeansStrategy

    val means =kmeans.train(sampleVectors(vectors),vectors,kmeansEta,kmeansMaxIterations,kmeansKernels,false)
    val results = clusterResults(means, vectors)
    printResults(results)

    val stop = System.currentTimeMillis()
    println(s"Processing took ${stop - start} ms.\n")
  }

}

class Application extends Serializable {
  val langs =
    List(
      "JavaScript", "Java", "PHP", "Python", "C#", "C++", "Ruby", "CSS",
      "Objective-C", "Perl", "Scala", "Haskell", "MATLAB", "Clojure", "Groovy")

  /** K-means parameter: How "far apart" languages should be for the kmeans algorithm? */
  def langSpread = 50000
  assert(langSpread > 0, "If langSpread is zero we can't recover the language from the input data!")

  /** K-means parameter: Number of clusters */
  def kmeansKernels = 45

  /** K-means parameter: Convergence criteria */
  def kmeansEta: Double = 20.0D

  /** K-means parameter: Maximum iterations */
  def kmeansMaxIterations = 120

  /** Sample the vectors */
  def sampleVectors(vectors: RDD[(Int, Int)]): Array[(Int, Int)] = {

    assert(kmeansKernels % langs.length == 0, "kmeansKernels should be a multiple of the number of languages studied.")
    val perLang = kmeansKernels / langs.length

    // http://en.wikipedia.org/wiki/Reservoir_sampling
    def reservoirSampling(lang: Int, iter: Iterator[Int], size: Int): Array[Int] = {
      val res = new Array[Int](size)
      val rnd = new util.Random(lang)

      for (i <- 0 until size) {
        assert(iter.hasNext, s"iterator must have at least $size elements")
        res(i) = iter.next
      }

      var i = size.toLong
      while (iter.hasNext) {
        val elt = iter.next
        val j = math.abs(rnd.nextLong) % i
        if (j < size)
          res(j.toInt) = elt
        i += 1
      }

      res
    }

    val res =
      if (langSpread < 500)
      // sample the space regardless of the language
        vectors.takeSample(false, kmeansKernels, 42)
      else
      // sample the space uniformly from each language partition
        vectors.groupByKey.flatMap({
          case (lang, vectors) => reservoirSampling(lang, vectors.toIterator, perLang).map((lang, _))
        }).collect()

    assert(res.length == kmeansKernels, res.length)
    res
  }

  def clusterResults(means: Array[(Int, Int)], vectors: RDD[(Int, Int)]): Array[(String, Double, Int, Int)] = {
    val closest = vectors.map(p => (KMeansHelper.findClosest(p, means), p))
    val closestGrouped = closest.groupByKey()


    val median = closestGrouped.mapValues { vs =>
      val langAndQuestions = vs.groupBy(_._1).map(t=> (t._1,t._2.size))
      val mostCommonLangPoints=langAndQuestions.maxBy(_._2)
      val langLabel: String   = { // most common language in the cluster
      val langKey = mostCommonLangPoints._1
        langs(langKey/langSpread)
      }
      val langPercent: Double = { // percent of the questions in the most common language
      val sum = langAndQuestions.foldLeft(0)((count,point)=>count+point._2)

        (mostCommonLangPoints._2.toDouble/sum.toDouble)*100
      }
      val clusterSize: Int    = vs.size

      val (lower:List[(Int,Int)],upper:List[(Int,Int)]) = vs.toList.sortBy(_._2).splitAt((clusterSize.toDouble/2).toInt)

      var myscore = upper.head._2
      if(clusterSize % 2 ==0 ){
        myscore = (lower.last._2+upper.head._2)/2
      }

      val medianScore: Int    =myscore

      (langLabel, langPercent, clusterSize, medianScore)
    }

    median.collect().map(_._2).sortBy(_._4)
  }

  def printResults(results: Array[(String, Double, Int, Int)]): Unit = {
    println("Resulting clusters:")
    println("  Score  Dominant language (%percent)  Questions")
    println("================================================")
    for ((lang, percent, size, score) <- results)
      println(f"${score}%7d  ${lang}%-17s (${percent}%-5.1f%%)      ${size}%7d")
  }
}
