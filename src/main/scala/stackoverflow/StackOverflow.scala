package stackoverflow

import com.company.analytics.models.Posting
import com.company.analytics.preprocessors.PreprocessorHelper
import org.apache.spark.{HashPartitioner, SparkConf, SparkContext}
import org.apache.spark.SparkContext._
import org.apache.spark.rdd.RDD
import annotation.tailrec
import scala.reflect.ClassTag

/** The main class */
object StackOverflow extends StackOverflow {

  @transient lazy val conf: SparkConf = new SparkConf()
    .setMaster("local[2]") // remove if running in cluster
    .setAppName("StackOverflow")
    .set("spark.serializer","org.apache.spark.serializer.KryoSerializer")
  conf.registerKryoClasses(Array(classOf[Posting]))
  @transient lazy val sc: SparkContext = new SparkContext(conf)

  /** Main function */
  def main(args: Array[String]): Unit = {
    val start = System.currentTimeMillis()

    val lines   = sc.textFile("src/main/resources/stackoverflow/stackoverflow.csv")
    val raw     = rawPostings(lines)
    val grouped = groupedPostings(raw)
   val scored  = scoredPostings(grouped)
 //   val scored = getScoredPostings(raw)
    val vectors = vectorPostings(scored)
//    assert(vectors.count() == 2121822, "Incorrect number of vectors: " + vectors.count())

    val means   = kmeans(sampleVectors(vectors), vectors, debug = false)
    val results = clusterResults(means, vectors)
    printResults(results)

    val stop = System.currentTimeMillis()
    println(s"Processing took ${stop - start} ms.\n")

  }
}


/** The parsing and kmeans methods */
class StackOverflow extends Serializable {

  /** Languages */
  val langs =
    List(
      "JavaScript", "Java", "PHP", "Python", "C#", "C++", "Ruby", "CSS",
      "Objective-C", "Perl", "Scala", "Haskell", "MATLAB", "Clojure", "Groovy")

  /** K-means parameter: How "far apart" languages should be for the kmeans algorithm? */
  def langSpread = 50000
  assert(langSpread > 0, "If langSpread is zero we can't recover the language from the input data!")

  /** K-means parameter: Number of clusters */
  def kmeansKernels = 45

  /** K-means parameter: Convergence criteria */
  def kmeansEta: Double = 20.0D

  /** K-means parameter: Maximum iterations */
  def kmeansMaxIterations = 120


  //
  //
  // Parsing utilities:
  //
  //

  /** Load postings from the given file */
  def rawPostings(lines: RDD[String]): RDD[Posting] =
    lines.map(line => {
      val arr = line.split(",")
      Posting(postingType =    arr(0).toInt,
              id =             arr(1).toInt,
              acceptedAnswer = if (arr(2) == "") None else Some(arr(2).toInt),
              parentId =       if (arr(3) == "") None else Some(arr(3).toInt),
              score =          arr(4).toInt,
              tags =           if (arr.length >= 6) Some(arr(5).intern()) else None)
    })


  /** Group the questions and answers together */
  def groupedPostings(postings: RDD[Posting]): RDD[(Int, Iterable[(Posting, Posting)])] = {
    val postingCached = postings.persist()
    val questionsPostings = postingCached.filter(_.postingType==1).map(posting=> (posting.id,posting))
    .partitionBy(new HashPartitioner(10)).persist()
    val answersPostings = postingCached.filter((post)=>(post.postingType==2) && (post.parentId!=None))
      .map(posting=>(posting.parentId.get, posting))
    postingCached.unpersist()
    val questionsAndAnswers = questionsPostings.join(answersPostings).groupByKey()
    questionsPostings.unpersist()
    questionsAndAnswers
  }


  /** Compute the maximum score for each posting */
  def scoredPostings(grouped: RDD[(Int, Iterable[(Posting, Posting)])]): RDD[(Posting, Int)] = {

    def answerHighScore(as: Array[Posting]): Int = {
      var highScore = 0
          var i = 0
          while (i < as.length) {
            val score = as(i).score
                if (score > highScore)
                  highScore = score
                  i += 1
          }
      highScore
    }

    grouped.mapValues((iterator:Iterable[(Posting,Posting)])=> {
      val question = iterator.head._1
      val score = answerHighScore(iterator.map(_._2).toArray)
      (question,score)
    }).map(_._2)
  }

  /**
    * THis funciton get scored posting directly from postings rdd without using group by, it looks for the max score per question by
    *
    * @param postings
    * @return
    */
  def getScoredPostings(postings: RDD[Posting]): RDD[(Posting, Int)]={
    postings.persist()
    val questionPostings = postings.filter(_.postingType==1).map(posting=> (posting.id,posting))
    val answerPostings = postings.filter((post)=>(post.postingType==2) && (post.parentId!=None))
      .map(posting=>(posting.parentId.get, posting))

      .partitionBy(new HashPartitioner(8)).persist // repartition so that reduce and join are less expensive
    postings.unpersist()
    val answerPostingsWithMaxSocre = answerPostings
      .reduceByKey((posting1,posting2)=>{
        if(posting1.score> posting2.score){
          posting1
        }else{
          posting2
        }
      }) // get question ID and answer with max score

    val scoredPostings = questionPostings.join(answerPostingsWithMaxSocre).
      map(questionAndAnswerWithMaxScore => {
        val question = questionAndAnswerWithMaxScore._2._1
        val answer = questionAndAnswerWithMaxScore._2._2
        (question,answer.score)
      })
    answerPostings.unpersist()
    scoredPostings
  }

  /** Compute the vectors for the kmeans */
  def vectorPostings(scored: RDD[(Posting, Int)]): RDD[(Int, Int)] = {
    /** Return optional index of first language that occurs in `tags`. */
    def firstLangInTag(tag: Option[String], ls: List[String]): Option[Int] = {
      if (tag.isEmpty) None
      else if (ls.isEmpty) None
      else if (tag.get == ls.head) Some(0) // index: 0
      else {
        val tmp = firstLangInTag(tag, ls.tail)
        tmp match {
          case None => None
          case Some(i) => Some(i + 1) // index i in ls.tail => index i+1
        }
      }
    }

    scored.flatMap((questionAndScore) => {
      val question = questionAndScore._1
      val tag = question.tags
      val index = PreprocessorHelper.firstLangInTag(tag,langs)
      index match {
        case None  => None
        case Some(v) =>  Some(index.get*langSpread,questionAndScore._2)
      }
    })
      .persist()
  }


  /** Sample the vectors */
  def sampleVectors(vectors: RDD[(Int, Int)]): Array[(Int, Int)] = {

    assert(kmeansKernels % langs.length == 0, "kmeansKernels should be a multiple of the number of languages studied.")
    val perLang = kmeansKernels / langs.length

    // http://en.wikipedia.org/wiki/Reservoir_sampling
    def reservoirSampling(lang: Int, iter: Iterator[Int], size: Int): Array[Int] = {
      val res = new Array[Int](size)
      val rnd = new util.Random(lang)

      for (i <- 0 until size) {
        assert(iter.hasNext, s"iterator must have at least $size elements")
        res(i) = iter.next
      }

      var i = size.toLong
      while (iter.hasNext) {
        val elt = iter.next
        val j = math.abs(rnd.nextLong) % i
        if (j < size)
          res(j.toInt) = elt
        i += 1
      }

      res
    }

    val res =
      if (langSpread < 500)
        // sample the space regardless of the language
        vectors.takeSample(false, kmeansKernels, 42)
      else
        // sample the space uniformly from each language partition
        vectors.groupByKey.flatMap({
          case (lang, vectors) => reservoirSampling(lang, vectors.toIterator, perLang).map((lang, _))
        }).collect()

    assert(res.length == kmeansKernels, res.length)
    res
  }


  //
  //
  //  Kmeans method:
  //
  //

  /** Main kmeans computation */
  @tailrec final def kmeans(means: Array[(Int, Int)], vectors: RDD[(Int, Int)], iter: Int = 1, debug: Boolean = false): Array[(Int, Int)] = {

    // Fill in the newMeans array
    val newMeansPoints = vectors.map(point =>{
      (findClosest(point,centers = means),(point._1.toLong,point._2.toLong,1))
    })

    val newMeansWithAvg = newMeansPoints
      .reduceByKey((pointAndCount1,pointAndCount2)=>{

        val sumX = pointAndCount1._1+pointAndCount2._1
        val sumY = pointAndCount1._2+pointAndCount2._2
        val count = pointAndCount1._3+pointAndCount2._3
        (sumX,sumY,count)
      })
      .mapValues(pointSumAndCount=>((pointSumAndCount._1/pointSumAndCount._3).toInt,(pointSumAndCount._2/pointSumAndCount._3).toInt))
      //.groupByKey()
      //.mapValues(points => KMeansHelper.averageVectors(points))
      .collect()
    val indexHelper = newMeansWithAvg.map(_._1)
    //
    val newMeans = means.zipWithIndex.map(valueWithIndex=>{
      val indexOfNewMeans = indexHelper.indexOf(valueWithIndex._2)
      if(indexOfNewMeans == -1){
        (valueWithIndex._2,valueWithIndex._1) // assign the old centroid if the new centroid does not have the index
      }else{
        newMeansWithAvg(indexOfNewMeans) // otherwise, the new centroid becomes the centroid in that index
      }
    }).map(_._2) // means and newMeans must have the same size

    val distance = euclideanDistance(means, newMeans)

    if (debug) {
      println(s"""Iteration: $iter
                 |  * current distance: $distance
                 |  * desired distance: $kmeansEta
                 |  * means:""".stripMargin)
      for (idx <- 0 until kmeansKernels)
      println(f"   ${means(idx).toString}%20s ==> ${newMeans(idx).toString}%20s  " +
              f"  distance: ${euclideanDistance(means(idx), newMeans(idx))}%8.0f")
    }

    if (converged(distance))
      newMeans
    else if (iter < kmeansMaxIterations)
      kmeans(newMeans, vectors, iter + 1, debug)
    else {
      println("Reached max iterations!")
      newMeans
    }
  }




  //
  //
  //  Kmeans utilities:
  //
  //

  /** Decide whether the kmeans clustering converged */
  def converged(distance: Double) =
    distance < kmeansEta


  /** Return the euclidean distance between two points */
  def euclideanDistance(v1: (Int, Int), v2: (Int, Int)): Double = {
    val part1 = (v1._1 - v2._1).toDouble * (v1._1 - v2._1)
    val part2 = (v1._2 - v2._2).toDouble * (v1._2 - v2._2)
    part1 + part2
  }

  /** Return the euclidean distance between two points */
  def euclideanDistance(a1: Array[(Int, Int)], a2: Array[(Int, Int)]): Double = {
    assert(a1.length == a2.length)
    var sum = 0d
    var idx = 0
    while(idx < a1.length) {
      sum += euclideanDistance(a1(idx), a2(idx))
      idx += 1
    }
    sum
  }

  /** Return the closest point */
  def findClosest(p: (Int, Int), centers: Array[(Int, Int)]): Int = {
    var bestIndex = 0
    var closest = Double.PositiveInfinity
    for (i <- 0 until centers.length) {
      val tempDist = euclideanDistance(p, centers(i))
      if (tempDist < closest) {
        closest = tempDist
        bestIndex = i
      }
    }
    bestIndex
  }


  /** Average the vectors */
  def averageVectors(ps: Iterable[(Int, Int)]): (Int, Int) = {
    val iter = ps.iterator
    var count = 0
    var comp1: Long = 0
    var comp2: Long = 0
    while (iter.hasNext) {
      val item = iter.next
      comp1 += item._1
      comp2 += item._2
      count += 1
    }
    ((comp1 / count).toInt, (comp2 / count).toInt)
  }




  //
  //
  //  Displaying results:
  //
  //
  def clusterResults(means: Array[(Int, Int)], vectors: RDD[(Int, Int)]): Array[(String, Double, Int, Int)] = {
    val closest = vectors.map(p => (findClosest(p, means), p))
    val closestGrouped = closest.groupByKey()

    val median = closestGrouped.mapValues { vs =>
      val langAndQuestions = vs.groupBy(_._1).map(t=> (t._1,t._2.size))
      val mostCommonLangPoints=langAndQuestions.maxBy(_._2)
      val langLabel: String   = { // most common language in the cluster
      val langKey = mostCommonLangPoints._1
        langs(langKey/langSpread)
      }
      val langPercent: Double = { // percent of the questions in the most common language
      val sum = langAndQuestions.foldLeft(0)((count,point)=>count+point._2)

        (mostCommonLangPoints._2.toDouble/sum.toDouble)*100
      }
      val clusterSize: Int    = vs.size

      val (lower:List[(Int,Int)],upper:List[(Int,Int)]) = vs.toList.sortBy(_._2).splitAt((clusterSize.toDouble/2).toInt)

      var myscore = upper.head._2
      if(clusterSize % 2 ==0 ){
        myscore = (lower.last._2+upper.head._2)/2
      }

      val medianScore: Int    =myscore

      (langLabel, langPercent, clusterSize, medianScore)
    }


    median.collect().map(_._2).sortBy(_._4)
  }

  def printResults(results: Array[(String, Double, Int, Int)]): Unit = {
    println("Resulting clusters:")
    println("  Score  Dominant language (%percent)  Questions")
    println("================================================")
    for ((lang, percent, size, score) <- results)
      println(f"${score}%7d  ${lang}%-17s (${percent}%-5.1f%%)      ${size}%7d")
  }
}
