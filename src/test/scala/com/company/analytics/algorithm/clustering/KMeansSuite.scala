package com.company.analytics.algorithm.clustering

import org.apache.spark.SparkContext
import org.scalatest.{FunSuite, BeforeAndAfterAll}
import stackoverflow.Application

/**
  * Created by zhang on 6/20/17.
  */
class KMeansSuite extends FunSuite with BeforeAndAfterAll with KMeansStrategy {
  var sc :SparkContext=_

  override def beforeAll(){
    sc = Application.sc
  }
  test("getClusters"){
    val inputVector = sc.parallelize(List((0,10),(50000,1),(0,0),(50000,5)))
    val sampleVector = Array((0,10),(50000,1))

    val clusterCentroids = kmeans(sampleVector,inputVector,5.0,2,100,false)
    val expectedCentroids = Array((0,5),(50000,3))
    assert(expectedCentroids === clusterCentroids)
  }


}
