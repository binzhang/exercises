package com.company.analytics.preprocessors

import com.company.analytics.models.Posting
import com.company.analytics.proprocessors.CSVPreprocessingExecutor
import org.apache.spark.SparkContext
import org.junit.runner.RunWith
import org.scalatest.{BeforeAndAfterAll, FunSuite}
import org.scalatest.junit.JUnitRunner
import stackoverflow.Application

/**
  * Created by zhang on 6/18/17.
  */
@RunWith(classOf[JUnitRunner])
class PreprocessorSuite extends FunSuite with BeforeAndAfterAll with CSVPreprocessingExecutor {
  var sc :SparkContext=_
  var postings:List[Posting]=_

  override def beforeAll(){
    sc = Application.sc
    postings = List(
      Posting(id=1,acceptedAnswer = None,parentId=None, score = 10,postingType=1,tags=Some("Java")),
      Posting(id=2,acceptedAnswer = None,parentId=None, score = 10,postingType=1,tags=Some("Scala")),
      Posting(id=3,acceptedAnswer = None,parentId=None, score = 10,postingType=1,tags=Some("Java")),
      Posting(id=4,acceptedAnswer = None,parentId=None, score = 10,postingType=1,tags=Some("Scala")),
      Posting(id=5,acceptedAnswer = None,parentId=Some(1), score = -2,postingType=2,tags=None),
      Posting(id=6,acceptedAnswer = None,parentId=Some(2), score = 0,postingType=2,tags=None),
      Posting(id=7,acceptedAnswer = None,parentId=Some(1), score = 10,postingType=2,tags=None),
      Posting(id=8,acceptedAnswer = None,parentId=Some(1), score = 5,postingType=2,tags=None),
      Posting(id=9,acceptedAnswer = None,parentId=Some(1), score = -2,postingType=2,tags=None),
      Posting(id=10,acceptedAnswer = None,parentId=Some(2), score = 0,postingType=2,tags=None),
      Posting(id=11,acceptedAnswer = None,parentId=Some(1), score = 3,postingType=2,tags=None),
      Posting(id=12,acceptedAnswer = None,parentId=Some(1), score = -2,postingType=2,tags=None),
      Posting(id=13,acceptedAnswer = None,parentId=Some(3), score = 0,postingType=2,tags=None),
      Posting(id=14,acceptedAnswer = None,parentId=Some(3), score = 0,postingType=2,tags=None),
      Posting(id=15,acceptedAnswer = None,parentId=Some(4), score = 5,postingType=2,tags=None),
      Posting(id=17,acceptedAnswer = None,parentId=Some(2), score = 1,postingType=2,tags=None),
      Posting(id=18,acceptedAnswer = None,parentId=Some(1), score = -10,postingType=2,tags=None)
    )
  }

  test("getScoredPostings "){


    val expectedScoredVector = List((postings(0),10),(postings(1),1),(postings(2),0),(postings(3),5)).sortBy(_._1.id)
    val rdd = sc.parallelize(postings)
    val resultScoredVector = getScoredPostings(rdd).collect().sortBy(_._1.id)
    assert(expectedScoredVector === resultScoredVector)

  }

  test("vectorPostings"){
    val scoredVector = List((postings(0),10),(postings(1),1),(postings(2),0),(postings(3),5))
    val inputRdd = sc.parallelize(scoredVector)
    val vector = vectorPostings(inputRdd,List("Java","Scala"),50000).collect().sortBy(_._1)

    val expectedVector = List((0,10),(50000,1),(0,0),(50000,5)).sortBy(_._1)
    assert(vector === expectedVector)

  }
}






